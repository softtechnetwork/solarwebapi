var Db = require('./dboperations');
var Customers = require('./customer');
const dboperations = require('./dboperations');

/** ( document - 2021/02/18 )
 * Express เป็น web application framework บน Node.js ที่ได้รับความนิยมมากๆตัวหนึ่ง 
 * ซึ่งตัว Express เนี่ยจะมีฟีจเจอร์ต่างๆที่ช่วยให้เราทำเว็บได้สะดวกขึ้น 
 * เช่น การทำ routing, middleware ใช้ในการจัดการ request และ response เป็นต้น 
 * ทำให้เราสามารถพัฒนาเว็บโดยใช้ Node.js ได้สะดวกและรวดเร็วยิ่งขึ้น
 * ติดตั้ง Express ด้วยคำสั่ง  ( npm install express --save )
 */
var express = require('express');

var bodyParser = require('body-parser');
var cors = require('cors');
var app = express();
var router = express.Router();

app.use(bodyParser.urlencoded({
   extended: true
})); // bodyParser.urlencoded({ extended: true }) จะทำให้เรา parse application/x-www-form-urlencoded ได้
app.use(bodyParser.json()); // ใช้ middleware ในการ parsing request body ครับ โดย bodyParser.json() จะทำให้เรา parse application/json

/*
Middleware คือ โค้ดที่ทำหน้าที่เป็นตัวกรอง request ก่อนที่จะเข้ามาถึงแอพพลิเคชั่นของเรา
ว่าง่ายๆคือก่อนที่จะเข้ามาถึง app.get หรือ app.post  
มันจะต้องผ่าน middleware ก่อน 
โดยเราสามารถใช้งาน middleware ได้ผ่าน app.use() 
เราเอา middleware ไปใช้ประโยชน์ได้หลายอย่าง เช่น กรอง request 
ว่าต้องมีการถือ token มาก่อน ถึงจะเข้ามาเอา resource ของเราได้ หรือเอาไว้เก็บ log ว่าใครเข้ามาที่แอพพลิเคชั่นของเราบ้าง
*/
app.use(cors());
app.use('/api', router);


router.use((request, response, next) => {
  
   console.log(request);
   next();
})


router.get("/Customers", async (req, res) => {

   try {
      //listing messages in users mailbox 
      dboperations.getCustomers().then(result => {
         res.json(result[0]);
      })
   } catch (err) {
      console.log(err)
   }
})

router.route('/Customers/:id').get((request, response) => {

   dboperations.getCustomer(request.params.id).then(result => {
      response.json(result[0]);
   })

})


router.get("/getdevices", async (req, res) => {

   try {
      //listing messages in users mailbox 
      dboperations.getdevices().then(result => {
         res.json(result[0]);
      })
   } catch (err) {
      console.log(err)
   }
})

router.route('/getdevices/:id').get((request, response) => {
   dboperations.getdevice(request.params.id).then(result => {
      response.json(result[0]);
   })

})




router.post('/getdevice_relate_serial', (req, res) => {
  var deviceId = req.body.deviceId;
   dboperations.getdevice_relate_serial(deviceId).then(result => {
      res.json(result[0]);
   })
   // res.sendStatus(200);
});


/**
 * จากโค้ดก็คือเราจะสร้าง server อยู่ที่ port 8090 จะทำให้เราเข้าถึง server 
 * ได้ที่ http://localhost:8090 ซึ่งเราสามารถสั่งรันได้ด้วยคำสั่ง
 */
var port = process.env.PORT || 8090;
app.listen(port);
console.log('Solar API is runnning at ' + port);