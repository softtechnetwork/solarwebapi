var config = require('./dbconfig');
const sql = require('mssql');

/**
 * =====================
 * API : แสดงรายการลูกค้า
 * =====================
 */
async function getCustomers() {
    try {
     
        let pool = await sql.connect(config);
        let customers = await pool.request().query("SELECT   * from customers");
      
        return customers.recordsets;
    } catch (error) {
       // console.log("error");
        console.log("error : " + error);
        
    }
}


async function getCustomer(id) {
    try {
        let pool = await sql.connect(config);
        let customer = await pool.request()
            .input('input_parameter', sql.Int, id)
            .query("SELECT * from customers where id = @input_parameter");
        return customer.recordsets;

    } catch (error) {
        console.log(error);
    }
}


/**
 * =====================
 * API : แสดงรายการอุปกรณ์
 * =====================
 */
 async function getdevices() {
    try {
        let pool = await sql.connect(config);
        let devices = await pool.request().query("SELECT * from devices");
        return devices.recordsets;
    } catch (error) {
        console.log(error);
    }
}

async function getdevice(id) {
    try {
        let pool = await sql.connect(config);
        let customer = await pool.request()
            .input('input_parameter', sql.Int, id)
            .query("SELECT * from devices where id = @input_parameter");
        return customer.recordsets;

    } catch (error) {
        console.log(error);
    }
}



/**
 * ==========================================
 * API : แสดง serial number ที่ผูกกับอุปกรณ์นี้
 * ==========================================
 */

async function getdevice_relate_serial(deviceId) {
    try {
        let pool = await sql.connect(config);
        let customer = await pool.request()
            .input('input_parameter', sql.VarChar, deviceId)
            .query("SELECT * from devices_rel_sn where deviceId = @input_parameter");
        return customer.recordsets;

    } catch (error) {
        console.log(error);
    }
}



async function getCustomerJoinStation(id) {
    try {
        let pool = await sql.connect(config);
        let customerjoinstation = await pool.request()
            .input('input_parameter', sql.Int, id)
            .query("SELECT customer.id,customers_rel_station.stationId from customers_rel_station on customer.id = customers_rel_station.customer_id join  where customer.id = @input_parameter");
        return customerjoinstation.recordsets;

    } catch (error) {
        console.log(error);
    }
}


async function addCustomer(Customer) {

    try {
        let pool = await sql.connect(config);
        let insertCustomer = await pool.request()
            .input('username', sql.NVarChar, Customer.username)
            .input('original_password', sql.NVarChar, Customer.original_password)
            .execute('InsertCustomer');
        return insertCustomer.recordsets;
    } catch (err) {
        console.log(err);
    }

}



module.exports = {
    getCustomers: getCustomers,
    getCustomer: getCustomer,
    getdevices : getdevices,
    getdevice : getdevice,
    getdevice_relate_serial: getdevice_relate_serial,
    getCustomerJoinStation: getCustomerJoinStation,
    addCustomer: addCustomer
}